function [s_appr, s_plot,t_plot] = walsh_trunk_approx(f, N, idx)
    
    samples = walsh_sampling(f,N);

    eps = 1e-14;
    t_plot = linspace(0, 1-eps, 4*N);
    t = linspace( 0, 1-eps, N);

    s_plot = 0;
    s_appr = 0;
    for n = idx
        s_plot = s_plot + samples(n)*wal(n-1, t_plot);
        s_appr = s_appr + samples(n)*wal(n-1, t);
    end
end

