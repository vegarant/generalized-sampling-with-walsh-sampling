R = 7;
q = 1;
q_K = 1;
vm = 3;
is_per = 0;
sigma = 0.001;
om_r1 = 0.1;
visible = 'off';
M = 2^R;
nbr_samples = round(0.65*M);

log2N = R+q;
log2M = R;
log2K = R+q_K;


nbr = 1;
K = 2^log2K;

if (is_per)
    is_per_str = 'per';
else
    is_per_str = 'bd';
end

%fname = 'f';
%
%% 1
%f = @(x) (x+1).*(x-0.5).*(x-0.25).*(x+3).*(x-0.5) + cos(2*pi*x); 
%
%is_per = 0;
%compare_recovery_techniques(f, log2N, log2M, log2K, vm, is_per, ...
%                            nbr_samples, sigma, om_r1, visible, 1, fname);
%
%is_per = 1;
%compare_recovery_techniques(f, log2N, log2M, log2K, vm, is_per, ...
%                            nbr_samples, sigma, om_r1, visible, 1, fname);
%
%% 2
f = @(x) cos(2*pi*x);% + 0.2*cos(10*pi*x);
fname = 'g';

is_per = 0;
compare_recovery_techniques(f, log2N, log2M, log2K, vm, is_per, ...
                            nbr_samples, sigma, om_r1, visible, 2, fname);

%is_per = 1;
%compare_recovery_techniques(f, log2N, log2M, log2K, vm, is_per, ...
%                            nbr_samples, sigma, om_r1, visible, 2, fname);
%
%
%
%% 3
%f = @(x) (x<0.5).*(x+1).*(x-0.5).*(x-0.25).*(x+3).*(x-0.5) + (x>= 0.5).*cos(2*pi*x); 
%fname = 'h';
%
%is_per = 0;
%compare_recovery_techniques(f, log2N, log2M, log2K, vm, is_per, ...
%                            nbr_samples, sigma, om_r1, visible, 4, fname);
%
%is_per = 1;
%compare_recovery_techniques(f, log2N, log2M, log2K, vm, is_per, ...
%                            nbr_samples, sigma, om_r1, visible, 3, fname);
%











