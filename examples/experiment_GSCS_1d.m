function [x, z] = experiment_GSCS_1d(A, f, log2N, log2M, vm, is_per, idx, scales, sigma)

    R = log2M;
    M = 2^R
    N = 2^log2N;
    q = log2N - R;
    spgl1_verbose = 1;

    %explicit = ~(isa(A,'function_handle'));

    samples = walsh_sampling(f,N);
    y = scales(idx).*samples(idx);

    opts = spgSetParms('verbosity', spgl1_verbose);
    z = spg_bpdn(A, y, sigma, opts); 

    J0 = computeJ0(vm);
    nres = R-J0;

    if (is_per)
        S  = get_wavedec_s(R, nres);
        sc = waverec(z, S, sprintf('db%d', vm));
        X  = get_scaling_matrix_per(vm, N, M);
    else 
        sc = IWT_CDJV_noP(z, J0, vm);
        X  = get_scaling_matrix_bd(vm, R, log2N);
    end

    x = X*sc; 
end
