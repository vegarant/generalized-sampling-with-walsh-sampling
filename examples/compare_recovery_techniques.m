function compare_recovery_techniques(f, log2N, log2M, log2K, vm, is_per, ...
                nbr_samples, sigma, om_r1, visible, nbr, fname)

    fsize  = 25;
    lwidth = 1.5;

    R = log2M;
    q = log2N - R;
    q_K = log2K - R;
    M = 2^R;
    N = 2^log2N;
    K = 2^log2K;

    J0 = computeJ0(vm);
    dwtmode('per', 'nodisp');

    %N,M,nbr_samples, J0
    %A2 = create_A1d_mat(vm, R+q+q_K, R+q_K, 1:2^(R+q+q_K), is_per);
    %cond(A2)
    [idx, scales] = sph1_rect3(N, M, nbr_samples, J0);

    if is_per
        A_handle = @(x,mode)  handle_GSCS_per(x, mode, vm, R+q+q_K, R+q_K, idx, scales);
    else
        A_handle = @(x,mode)  handle_GSCS_bd( x, mode, vm, R+q+q_K, R+q_K, idx, scales);
    end


    [x2, z2] = experiment_GSCS_1d_w_K(A_handle, f, R+q, R, ...
                                R + q_K,vm, is_per, idx, scales, sigma, om_r1);

    [s_approx, s_plot, t_plot] = walsh_trunk_approx(f,N, 1:nbr_samples);
    t = linspace(0,1-1e-14,N);
    y = f(t)';

    fig = figure('Visible', 'on');
    plot(t,f(t), 'Linewidth', lwidth);
    %title(sprintf('vm: %d, N: %d, M: %d, samples: %d', vm, N, M, nbr_samples), 'FontSize', fsize);

    hold('on');
    %plot(t, x1);
    plot(t_plot, s_plot, 'Linewidth', lwidth);
    plot(t, x2, 'Linewidth', lwidth);

    set(gca,'fontsize', fsize)

    legend({fname, sprintf('%s_m', fname), sprintf('%s_{N,K,m}', fname)}, ...
           'Orientation', 'vertical', 'Location', 'southwest',  ...
           'FontSize', fsize);
    
    if (is_per)
        is_per_str = 'per';
    else
        is_per_str = 'bd';
    end
    
    fprintf('nbr: %2d, bd_mode: %3s ||f - f_m ||_1: %g \n', nbr,...
             is_per_str, norm(f(t)- s_approx, 1));
    fprintf('nbr: %2d, bd_mode: %3s ||f - f_{N,K,m}||_1: %g \n', nbr,...
             is_per_str, norm(f(t)- x2', 1));

    filename = sprintf('../plots/GSCS_rec%d_vm%d_samp%d_N%d_M%d_K%d_%s.png',...
                       nbr, vm, nbr_samples, N, M, K, is_per_str);

    saveas(fig, filename);



end




