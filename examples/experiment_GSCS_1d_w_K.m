function [x, z] = expermiment_GSCS_1d_w_K(A, f, log2N, log2M, log2K, vm, ...
                     is_per, idx, scales, sigma, om_r1)

    K = 2^log2K;
    N = 2^log2N;
    R = log2M;
    M = 2^R;
    q = log2N - R;
    q_K = log2K - R;    

    spgl1_verbose = 1;

    %explicit = ~(isa(A,'function_handle'));

    samples = walsh_sampling(f,N);
    y = scales.*samples(idx);

    %A = A*diag([ones(1,M), om_r1*ones(1,K-M)]);

    opts = spgSetParms('verbosity', spgl1_verbose);
    z = spg_bpdn(A, y, sigma, opts); 

    J0 = computeJ0(vm);
    nres = log2K-J0;

    if (is_per)
        S  = get_wavedec_s(log2K, nres);
        sc = waverec(z, S, sprintf('db%d', vm));
        X  = get_scaling_matrix_per(vm, 2^(R+q+q_K), 2^(R+q_K));
    else 
        sc = IWT_CDJV_noP(z, J0, vm);
        X  = get_scaling_matrix_bd(vm, log2K, log2N+q_K);
    end

    x = X*sc;
    x = x(1:2^q_K:2^(R+q+q_K)); 

end


