fsize = 25;
dest = '../plots/';
tic
R = 6;
q = 1;
vm = 3;
is_per = 0;
om_r1 = 0.1;
sigma = 0.01;
nbr = 2;

M = 2^R;
N = 2^(R+q);
log2N = R+q;
log2M = R;
J0 = computeJ0(vm);

nbr_samples = round(M*M/4);

t = linspace(0,1,N);

[X,Y] = meshgrid(t,t);

%f = @(x,y) 5*(x-1).*(x-0.5).*(x-0.25).*cos(4*pi*y) + 5;
f = @(x,y) 10*cos(1.5*pi*x).*sin(3*pi*y);
F = f(X,Y);

samples = walsh_sampling2(f, N);
[idx, scales] = sph2_rect(N, nbr_samples);

y = scales(idx).*samples(idx);

[Q, z] = experiment_GSCS_2d(f, log2N, log2M, vm, is_per, idx, scales, sigma, samples);

[S_approx, S_plot,t_plot] = walsh_trunk_approx2(f, N, nbr_samples, samples);

figure()
subplot(2,2,1);
imagesc(F);
title('Original');
colormap('jet')
colorbar();

subplot(2,2,2);
V = zeros(N,N);
V(idx) = scales(idx);
imagesc(V);
title('Sampling Pattern');
colormap('jet')
colorbar();

subplot(2,2,3);
imagesc(Q);
title('Recovery');
colormap('jet')
colorbar();

subplot(2,2,4);
imagesc(S_plot);
title('Walsh trunkcation');
colormap('jet')
colorbar();

norm(Q-F,'fro')
norm(S_approx - F, 'fro')
toc

%fname_org = sprintf('%sGSCS2d_%dorig.png', dest, nbr);
%F_scaled = ( F - min(F(:)) )/(max(F(:)) - min(F(:)));
%imwrite(uint8(255*F_scaled), fname_org);
%
%
%fname_rec = sprintf('%sGSCS2d_%drec_vm%d_log2N%d_log2M%d_samp%d.png', dest, ...
%                    nbr, vm, log2N, log2M, nbr_samples);
%Q_scaled = ( Q - min(Q(:)) )/(max(Q(:)) - min(Q(:)));
%imwrite(uint8(255*Q_scaled), fname_rec);
%
%
%fname_trunk = sprintf('%sGSCS2d_%dtrunk_vm%d_log2N%d_log2M%d_samp%d.png', dest,...
%                      nbr, vm, log2N, log2M, nbr_samples);
%S_approx_scaled = ( S_approx - min(S_approx(:)) )/(max(S_approx(:)) - min(S_approx(:)));
%imwrite(uint8(255*S_approx_scaled), fname_trunk);
%
%fname_patt = sprintf('%sGSCS2d_%dpatt_vm%d_log2N%d_log2M%d_samp%d.png', dest, ...
%                     nbr, vm, log2N, log2M, nbr_samples);
%V = zeros([N,N], 'uint8');
%V(idx) = 255;
%imwrite(V, fname_patt);





