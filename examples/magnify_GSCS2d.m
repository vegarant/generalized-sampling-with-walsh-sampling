% This file read in images created from example_GSCS_2d_oneRun
% and cut out the most interesting parts.

dest = '../plots/';
R = 6;
q = 1;
vm = 3;
nbr = 2;

M = 2^R;
N = 2^(R+q);
log2N = R+q;
log2M = R;
nbr_samples = round(M*M/4);

% x and y coordinates in the interval [0,1]
x = 0.5; 
y = 0.40;

cx = round(N*x);
cy = round(N*y)

box_size = 2^(R+q-1);

if (cx - box_size/2 + 1 < 0)
    cxl = 1;
    cxh = box_size;
elseif( cx+box_size/2 > N )
    cxh = N;
    cxl = N-box_size+1;
else 
    cxl = cx - box_size/2 + 1;
    cxh = cx + box_size/2;
end

if (cy - box_size/2 + 1 < 0)
    cyl = 1;
    cyh = box_size;
elseif( cy+box_size/2 > N )
    cyh = N;
    cyl = N-box_size+1;
else 
    cyl = cy - box_size/2 + 1;
    cyh = cy + box_size/2;
end
cxl
cxh
cyl
cyh
fname_org = sprintf('%sGSCS2d_%dorig_log2N%d_log2M%d.png', dest, nbr, log2N,...
                    log2M);
fname_rec = sprintf('%sGSCS2d_%drec_vm%d_log2N%d_log2M%d_samp%d.png', dest, ...
                    nbr, vm, log2N, log2M, nbr_samples);
fname_trunk = sprintf('%sGSCS2d_%dtrunk_vm%d_log2N%d_log2M%d_samp%d.png', dest,...
                      nbr, vm, log2N, log2M, nbr_samples);
im_org = imread(fname_org);
im_rec = imread(fname_rec);
im_trunk = imread(fname_trunk);

fname_org_box = sprintf('%sGSCS2d_box_%dorig.png', dest, nbr);
fname_rec_box = sprintf('%sGSCS2d_box_%drec_vm%d_log2N%d_log2M%d_samp%d.png', dest, ...
                    nbr, vm, log2N, log2M, nbr_samples);
fname_trunk_box = sprintf('%sGSCS2d_box_%dtrunk_vm%d_log2N%d_log2M%d_samp%d.png', dest,...
                      nbr, vm, log2N, log2M, nbr_samples);


imwrite(im_org(cyl:cyh, cxl:cxh), fname_org_box);
imwrite(im_rec(cyl:cyh, cxl:cxh), fname_rec_box);
imwrite(im_trunk(cyl:cyh, cxl:cxh), fname_trunk_box);


