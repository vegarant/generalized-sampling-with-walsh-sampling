clear all;
fsize = 25;

%rng(3);

R = 6;
q = 1;
q_K = 2;

M = 2^R;
N = 2^(R+q);
is_per = 1;
vm = 3;
J0 = computeJ0(vm);
sigma = 0.03;
spgl1_verbose = 1;
om_r1 = 10.1;

nbr_samples = 30;

[idx, scales] = sph1_rect1(N, N, nbr_samples, J0);

dwtmode('per', 'nodisp');

A1 = create_A1d_mat(vm, R+q, R+0, idx, is_per, scales);
U2 = create_U_matrix_discrete(N, vm, R+q-J0);
A2 = U2(idx,:);
f = @(x) cos(2*pi*x)  + 0.2 * cos(2*pi*4 *x); 
%f = @(x) (x+1).*(x-0.5).*(x-0.25).*(x+3).*(x-0.6) + cos(2*pi*x); 

samples = walsh_sampling(f,N);
t = linspace(0,1,N)';
samples2 = fastwht(f(t))*sqrt(N);
y = scales.*samples(idx);
y_noscale = samples(idx);
y2 = samples2(idx);

% Solve basis pursuit: min ||z||_1  s.t. Az = y
% Using linear programming
B1 = [A1, -A1];
c1 = ones(2*M,1);
lb1 = zeros(2*M,1);
[zz1, fval, exitflag1, options] = linprog(c1, [], [],B1, y, lb1, []);
z1 = zz1(1:M) - zz1(M+1:2*M);

B2 = [A2, -A2];
c2 = ones(2*N,1);
lb2 = zeros(2*N,1);
[zz2, fval, exitflag2, options] = linprog(c2, [], [],B2, y2, lb2, []);
z2 = zz2(1:N) - zz2(N+1:2*N);

S = get_wavedec_s(R+q, R+q-J0);
x2 = waverec(z2, S, sprintf('db%d', vm));

%opts = spgSetParms('verbosity', spgl1_verbose);
%z1 = spg_bpdn(A1, y, sigma, opts); 

nres = R-J0;

if (is_per)
    S = get_wavedec_s(R, nres);
    sc1 = waverec(z1, S, sprintf('db%d', vm));
    X1 = get_scaling_matrix_per(vm, N, M);
else 
    sc1 = IWT_CDJV_noP(z1, J0, vm);
    X1  = get_scaling_matrix_bd(vm, R, R+q);
end

x1 = X1*sc1; 
lwidth = 2;
fsize = 20;
fig = figure('Visible', 'on');
eps = 1e-14;
t = linspace(0,1-eps,N)';
t1 = linspace(0,1-eps,4*N);
plot(t1,f(t1), 'linewidth', lwidth);
%title(sprintf('vm: %d, N: %d, M: %d, samples: %d', vm, N, M, nbr_samples), 'FontSize', fsize);

hold('on');
plot(t, x1, 'linewidth', lwidth);


% Truncated Walsh series
s_plot = 0;
s_err = 0;
for n = 1:nbr_samples
    s_plot = s_plot + samples(n)*wal(n-1, t1);
    s_err = s_err + samples(n)*wal(n-1, t);
end


hold('on');
plot(linspace(0,1,length(x2)), x2, 'linewidth', lwidth);
set(gca,'fontsize',fsize)

legend({'f(t)', 'Inf-dim', 'Fin-dim'},'Location', 'southwest', 'FontSize', fsize);

saveas(fig, 'inf_vs_fin.png')

%fprintf('||f - f_N ||_1: %g \n', norm(f(t)- s_err', 1));
%fprintf('||f - f_{N,M,m}K||_1: %g \n', norm(f(t)- x1, 1));





