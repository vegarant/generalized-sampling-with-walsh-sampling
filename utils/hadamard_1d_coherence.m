function A = hadamard_1d_coherence(log2N, vm)
    %nu = 6;
    %nres = 4;
    %vm = 7;
    
    fsize = 20;
    N = 2^log2N;
    log2M = log2N;
    dwtmode('per', 'nodisp');
    
    is_per = 1;
    
    
    A = create_A1d_mat(vm, log2N, log2M, 1:N, is_per);    
 
    fig = figure('Visible', 'off'); 
    imagesc(abs(A));
    %title(sprintf('nu: %d, nres: %d,  vm: %d',nu, nres, vm));
    set(gca,'XTickLabel','', 'YTickLabel', '');
    colormap('jet')
    cba = colorbar();
    set(cba, 'fontsize', fsize);
    axis('square');
    y = round(100*linspace(min(abs(A(:))), max(abs(A(:))), 6))/100
    set(cba, 'YTick', y);
    
    saveas(fig, sprintf('../plots/hadamard_cont_1d_dim%d_db%d.png', N, vm));
end





