% IMPROVE: Need proper documentation
%
%
% x     - Signal coefficients 
% mode  - 1: forward transform, 2: adjoint operation 
% vm    - Number of vanishing moments
% log2N - 2^log2N = N
% log2M - 2^log2M = M
%
function y = handle_GSCS_bd(x, mode, vm, j0, log2N, log2M, idx, scales)

    if (or(mode == 1 , strcmpi('notransp', mode)))

        y = handle_GS_bd(x, mode, vm, j0, log2N, log2M);
        y = y(idx); 
        if (nargin > 7)
            y = scales.*y;
        end
    else % mode ~= 1 
        N = 2^log2N;
        if (nargin > 7)
            x = scales.*x;
        end
        z = zeros(N,1);
        z(idx) = x;
        y = handle_GS_bd(z, mode, vm, j0, log2N, log2M);

    end
end

