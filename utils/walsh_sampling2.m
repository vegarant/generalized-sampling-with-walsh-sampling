% Samples the function f: [0,1)^2 -> R with walsh functions as the integral
% transform
%            Wf(n,m) = int_{0}^{1} int_{0}^{1} f(x,y)w_n(x)w_m(y) dx dy
%
% Here N-1 is the maximum frequency and Omega is a subset of {0, ..., N-1} 
%
% INPUT
% f     - Function f : [0,1)^2 -> R 
% N     - N-1 is the maximum frequency
% Omega - two columns with a subset of {0,...,N-1}, if no argument is provided Omega is chosen as 
%         Omega = [(0:N-1)', (0:N-1)'].
%
% OUTPUT
% The walsh samples specified in Omega.
%
function samples = walsh_sampling2(f, N)

    Omega  = [(0:N-1)', (0:N-1)'];
    eps = 1e-14;
    samples = zeros(size(Omega,1), size(Omega,1));

    int_factor = 4;
    t = linspace(0,1-eps, int_factor*N);
    [X,Y] = meshgrid(t,t);
    F = f(X,Y);
    i_n = 1;
    i_m = 1;
    disp('Sampling in walsh frequency');
    for n = Omega(:,1)'
        wn = wal(n, t)';
        i_m = 1;
        for m = Omega(:,2)'
            %fprintf('i_n: %2d, i_m: %2d\n', i_n, i_m);
            wm = wal(m,t);
            W = wn*wm; % The outer product
            S = W.*F;
            samples(i_m, i_n) = sum(S(:))/((int_factor^2)*(N^2));
            i_m = i_m + 1;
        end
        progressbar(i_n,N);
        i_n = i_n + 1;
    end

end

