function y = handle_GSCS2_per(x, mode, vm, j0, log2N, log2M, idx, scales)

    if (or(mode == 1 , strcmpi('notransp', mode)))

        y = handle_GS2_per(x, mode, vm, j0, log2N, log2M);
        if (nargin > 7)
            y = y(idx).*scales(idx);
        else
            y = y(idx);
        end

    else % mode ~= 1 

        X = zeros(2^(2*log2N), 1);
        if (nargin > 7)
            X(idx) = scales(idx).*x;
        else
            X(idx) = x;
        end
        y = handle_GS2_per(X, mode, vm, j0, log2N, log2M);

    end

end




  

