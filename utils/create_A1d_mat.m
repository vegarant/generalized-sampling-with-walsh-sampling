function A = create_A1d_mat(vm, j0, log2N, log2M, idx, is_per, scales)

    q = log2N - log2M;
    R = log2M;
    M = 2^R;     % M - Number of wavelet coefficients
    N = 2^log2N; % N - Number of samples 

    I = eye(M);
    G = zeros(N,M);
    if (is_per)
        G_handle  = @(x) handle_GS_per   (x, 1, vm, j0, R+q, R);
    else
        G_handle  = @(x) handle_GS_bd(x, 1, vm, j0, R+q, R);
    end    
    for i = 1:M
        G(:, i) = G_handle(I(:,i));
        progressbar(i,M);
    end
    if (nargin == 7)
        A = diag(scales)*G(idx, :);
    elseif(nargin == 6)
        A = G(idx, :);
    end
end






