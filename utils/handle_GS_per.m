%
% x     - Signal coefficients 
% mode  - 1: forward transform, 2: adjoint operation 
% vm    - Number of vanishing moments
% log2N - 2^log2N = N
% log2M - 2^log2M = M
%
% Note: I might need to change this, so that various values for J0 is accepted
function y = handle_GS_per(x, mode, vm, j0, log2N, log2M, phi_walsh_pisces)

    % Store current boundary extension mode
    boundary_extension = dwtmode('status', 'nodisp');
    % Set the correct boundary extension for the wavelets
    dwtmode('per', 'nodisp');

    wname = sprintf('db%d', vm);
    
    is_per = 1;
    N = 2^log2N;
    M = 2^log2M;
    R = log2M; 
    nres = R - j0;
    q = log2N - R;

    if (nargin < 7)
        phi_walsh_pisces = get_phi_walsh_pisces(vm, j0, log2N, log2M, is_per);
    end

    if (or(mode == 1 , strcmpi('notransp', mode)))
    
        S = get_wavedec_s(R, nres);
        x = waverec(x, S, wname);
        y = kernel_GS_per(x, mode, vm, log2N, log2M, phi_walsh_pisces);
   
    else
        
        c = kernel_GS_per(x, mode, vm, log2N, log2M, phi_walsh_pisces);
        [y, S] = wavedec(c, nres, wname);
    
    end 
    
    % Restore dwtmode
    dwtmode(boundary_extension, 'nodisp')

end


