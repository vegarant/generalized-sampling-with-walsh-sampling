% This file tries to  detect your 'startup.m' file and add the requiered
% gs_walsh paths as default. It also add these paths to the current session.
% If it fails, you will have to find the 'startup.m' file 
% and add the requiered paths manually. 
%

dependent_paths = {'handles', 'utils', 'wave_lab_files', 'wave_tools', 'samp_path'};

us_root = userpath();
startup_file = fullfile(us_root, 'startup.m'); 

if (exist(us_root) == 7 )
    
    fID = fopen(startup_file, 'a');
    
    fprintf(fID, strcat(...
                 '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%', ...
                 '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n', ...
                 '%% These paths have automatically been added by the\n', ... 
                 '%% add_gs_walsh_path_permanently.m belonging to gs_walsh\n'));
    
    for gs_walsh_path = dependent_paths 
        fprintf(fID, 'addpath %s\n', fullfile(pwd(), gs_walsh_path{1}));
    end
    fprintf(fID, strcat( ... 
                 '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%', ...
                 '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n'));
    fclose(fID);
    fprintf('gs_walsh have added the following paths:\n');
    for gs_walsh_path = dependent_paths 
        fprintf('addpath %s\n', fullfile(pwd(), gs_walsh_path{1}));
        addpath(sprintf('%s', fullfile(pwd(), gs_walsh_path{1})))
    end
    fprintf(strcat(...
                 'to the file ', ... 
                 ' %s\n'), startup_file);
else 
    fprintf('gs_walsh was unable to find the file startup.m\n');        
    fprintf('in your `userpath()`.');
    fprintf('Adding the following paths to the current session');
    for gs_walsh_path = dependent_paths 
        fprintf('addpath %s\n', fullfile(pwd(), gs_walsh_path{1}));
        addpath(sprintf('%s', fullfile(pwd(), gs_walsh_path{1})));
    end
end

