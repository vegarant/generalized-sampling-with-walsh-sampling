

% Comptues the wavelet coefficients of the orthonormal wavelet with N
% vanishing moments and minimum phase.
%
% This is a copy paste function, from something I have made earlier, so some 
% of the computations might be redundant. 

function g0=compute_low_pass_filter(N)
    % Comptues the wavelet coefficients of the orthonormal wavelet with N
    % vanishing moments and minimum phase.
    %
    
    currDWTmode = dwtmode('status', 'nodisp');
    dwtmode('per','nodisp');
    nu = 7;
    n = 2^nu;
    x = zeros([1,n]);
    x(ceil(N/2)) = 1;
    
    S = [2^(nu-1); 2^(nu-1); n]; % compute the S given by wavedec
    wave_name = sprintf('db%d', N);
    
    y = waverec(x, S, wave_name);
    if (mod(N,2) == 1) % is odd
        g0 = y(1:2*N);
    else % is even 
        g0 = [y(end), y(1:2*N-1)];
    end
    
    %h0=fliplr(g0);
    %g1=h0.*(-1).^(0:(length(g0)-1)); 
    %h1=fliplr(g1);
    
    dwtmode(currDWTmode, 'nodisp');

end


% 0.230377813309
% 0.714846570553
% 0.630880767930
%-0.027983769417
%-0.187034811719
% 0.030841381836
% 0.032883011667
%-0.010597401785

