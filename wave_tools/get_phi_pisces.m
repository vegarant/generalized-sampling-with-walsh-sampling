% vm - Number of vanishing momoents
% nu - 2^r is the size of each pisce
% is_per - boolean. If true, pisces is a cell array with the 2*vm-1 unit pisces
% of phi. If false, pisces is a 2*vm+1 times 2*vm-1, where the rows correpsonds
% to the different functions, and columns consists of the unit pisces. Notice
% that some of the entries will be empty due to the different supports.  
%
function pisces = get_phi_picses(vm, j0, r, is_per);
    
    dim = r + j0;
    N = 2^(r + j0);
    n = 2^r;
    
    if (is_per)
        
        % Store current boundary extension mode
        boundary_extension = dwtmode('status', 'nodisp');
        % Set the correct boundary extension for the wavelets
        dwtmode('per', 'nodisp');
        
        nres = r;
        
        wname = sprintf('db%d', vm);
        S = get_wavedec_s(dim, nres);
        
        phi = zeros([N,1]);
        phi(vm) = 2^((r)/2);    

        phi = waverec(phi, S, wname);

        pisces = cell(1,2*vm - 1);

        for k = 1:2*vm-1
            pisces{k} = phi( ((k-1)*2^r) + (1:2^r) );
        end

        % Restore dwtmode
        dwtmode(boundary_extension, 'nodisp')

    else

        pisces = cell(2*vm + 1, 2*vm-1);

        for k = 1:vm

            ei_left = zeros([N,1]);
            ei_left(k) = 2^(r/2);
            wc_left = IWT_CDJV_noP(ei_left, j0, vm);

            ei_right = zeros([N,1]);
            ei_right(2^(j0)-k+1) = 2^(r/2);
            wc_right = IWT_CDJV_noP(ei_right, j0, vm);
             
            for i = 1:vm+k-1
                pisces{k,i} = wc_left( n*(i-1) + (1:n));
                pisces{k+vm+1,i} = wc_right( N-n*(vm+k-1) + n*(i-1) + (1:n));
            end 
        end

        ei = zeros([N,1]);
        ei(vm+1) = 2^(r/2);
        wc = IWT_CDJV_noP(ei, j0, vm); % Notice that the first 2^r coefficients are 0

        for i = 1:2*vm-1
            pisces{vm+1,i} = wc(n*i + (1:n));
        end

    end
    
end




%% Example program used for debugging

% vm = 2;
% R = 5;
% q = 4;
% r = 4;
% is_per = 0;
% 
% 
% pisces = get_phi_pisces(vm, r, is_per);
% 
% x_left = zeros((vm+k-1)*2^r,1);
% x_right = zeros((vm+k-1)*2^r,1);
% for l = 1:vm+(k-1)
%     x_left(idx + 2^r*(l-1)) =  pisces{k,l}; 
%     x_right(idx + 2^r*(l-1)) =  pisces{vm+1+k,l}; 
% end
% 
% subplot(1,2,1);
% plot(x_left)
% title('left');
% subplot(1,2,2);
% plot(x_right)
% title('right');


 
