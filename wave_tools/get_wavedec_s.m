function s = get_wavedec_s(dim, nres) 
    s = 2.^[dim-nres, (dim-nres):dim];
end

