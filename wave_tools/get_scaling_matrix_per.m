

% Note: In future versions this function should be replaced by a vectorized 
% loop, doing the matrix multiplicaition inplace. 
function A = get_scaling_matrix_per(vm, N, M)
    is_per = 1;
    if (2*M > N)
        disp('Error: N >= 2*M');
    end
    
    j     = floor(log2(M));
    log2N = floor(log2(N));
    
    L = log2N - j;
    
    [~, phi] = get_scaling_fn(vm, L, is_per);
    
    x = zeros([N,1]);
    x(1:2^L * vm) = phi(2^L*(vm-1)+1:2^L*(2*vm-1));
    x(N-2^L*(vm-1)+1:N) = phi(1:2^L*(vm-1));

    A = zeros([N,M]);
    
    for i = 1:M
        A(:,i) = circshift(x, 2^L*(i-1));
    end
    A = 2^(j/2)*A;
end

% The math
% 
% Let g = ceil(log2(2*vm));
% 
% Each scaling function is non-zero on 2^(g-j). This means that each scaling
% function requiers 2^(log2N - j) samples, meaning that each unit interval of
% the phi function gets 2^(log2N +g -j -g) = 2^(log2N - j) samples i.e. 
% L = log2N - j
%




